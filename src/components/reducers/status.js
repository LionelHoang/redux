var initialState = false;
var Status = (state = initialState, action) => {
	if(action.type === 'TOOGLE_STATUS') {
		state = !state;
	}
	return state;
}
export default Status;