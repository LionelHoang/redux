var initialState = {
	by: 'sort',
	value: 1
}
var Sort = (state = initialState, action) => {
	if(action.type === 'SORT') {
		var { by, value } = action.sort;
		return {
			by,
			value
		}
	}
	return state;
}
export default Sort;