import Status from './status';
import Sort from './sort';
import {combineReducers} from 'redux';
const myReducer = combineReducers({
	Status,
	Sort
});
export default myReducer;